# FeatherRegistry

This registry allows you to use private packages from Feather in Julia 1.x. It 
is created using the [LocalRegistry.jl](https://github.com/GunnarFarneback/LocalRegistry.jl) 
package.

## Usage
If you're using at least Julia 1.1, then you can add this registry in the Pkg REPL
using
```
(v1.3) pkg> registry add https://gitlab.com/feather-ecosystem/FeatherRegistry.git
```

Once setup, you can use public packages as if they are standard public packages 
from the general Julia package registry. 

As described in [Pkg.jl](https://julialang.github.io/Pkg.jl/v1/managing-packages/) 
packages can be added in one of two ways. If you simply want to use the functionality 
from a package the you can `add` a package
```
(v1.3) pkg> add PkgRelease
```
If you want to extend upon existing functionality then choose `develop`
```
(v1.3) pkg> develop SortedSequences
```

## Registering your package with FeatherRegistry
You can register your package using the ``PkgRelease`` package. See the README
of  ``PkgRelease`` for additional details.
